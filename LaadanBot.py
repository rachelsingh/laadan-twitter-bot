# -*- coding: utf-8 -*-

import random
import tweepy, time, sys
import api_info
import subprocess

class LaadanBot:
	def __init__( self ):
		self.api = None
		self.waitMinutes = 120
		self.offline = False
		self.lastWordOfTheDay = None
		
	def Initialize( self ):
		print( "LaadanBot:\t Initialize" )
		
		if ( self.offline ):
			print( "LaadanBot:\t I'm currently in offline mode." )
			
		else:
			auth = tweepy.OAuthHandler( api_info.consumerKey, api_info.consumerSecret )
			auth.set_access_token( api_info.accessKey, api_info.accessSecret )
			self.api = tweepy.API( auth )

	def Run( self ):
		while ( True ):
			self.DecideWhatToDo()
			
			print( "LaadanBot:\t Sleep for " + str( self.waitMinutes ) + " minutes..." )
			
			#for i in range( 1, self.waitMinutes )
			time.sleep( 60 * self.waitMinutes )
	
	def DecideWhatToDo( self ):
		choice = random.randint( 1, 4 )
		
		if 		( choice == 1 ):		print( "1" )
		elif 	( choice == 2 ):		print( "2" )
		elif 	( choice == 3 ):		print( "3" )
		elif 	( choice == 4 ):		print( "4" )
		
	def WordOfTheDay( self ):
		print( "LaadanBot:\t Run \"Word of the day\"..." )
		
	def Phrasebook( self ):
		print( "LaadanBot:\t Run \"Phrasebook\"..." )
		
	def RandomPhrase( self ):
		print( "LaadanBot:\t Run \"Random phrase\"..." )
		
	def Lesson( self ):
		print( "LaadanBot:\t Run \"Lesson\"..." )
